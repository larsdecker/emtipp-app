import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormBuilder, Validators} from "@angular/common";
import {ApiProvider} from "../../providers/api-provider/api-provider";

/*
  Generated class for the LoginPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/login/login.html',
  providers: [ApiProvider]
})
export class LoginPage {
  private loginForm;

  constructor(form: FormBuilder, api: ApiProvider) {
    console.log(api.load());

    // Create a new form group
    this.loginForm = form.group({ // name should match [ngFormModel] in your html
      username: ["", Validators.required], // Setting fields as required
      password: ["", Validators.required]
    });
  }

  private login(event:Event) {
    var test = api.login(this.loginForm.value.username, this.loginForm.value.password);
    console.log(test); // {username: <usename>, password: <password> }
    event.preventDefault();
  }
}
